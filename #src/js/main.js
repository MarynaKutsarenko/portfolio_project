(function () {
   const burgerBtn = document.querySelector('.btn_menu');
   const menuList = document.querySelector('.items');

   burgerBtn.addEventListener('click', function () {
      if (this.classList.contains('btn_menu-active')) {
         this.classList.remove('btn_menu-active');
          menuList.style.display = 'none';
      } else {
         this.classList.add('btn_menu-active');
         menuList.style.display = 'block';
      }
   })
}())